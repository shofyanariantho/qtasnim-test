@extends('layouts.admin.app')
@section('title', 'Transaction')

@section('content')
<div class="row">
    <div class="col-md-9 mx-md-auto">
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between align-items-center">
                <div class="card-title mb-0">
                    <h5 class="mb-0">Create @yield('title')</h5>
                    <small class="text-muted">Buat @yield('title')</small>
                </div>
                <a href="{{ route('transaction.index') }}" class="btn p-0" title="Kembali">
                    <i class="ti ti-x ti-sm text-muted"></i>
                </a>
            </div>

            <div class="card-body">
                <form action="{{ route('transaction.store') }}" method="POST">
                    @csrf

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Terdapat beberapa masalah dengan inputan Anda.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="row">
                        <div class="mb-3 col-md-5">
                            <label for="date" class="form-label">Tanggal</label>
                            <input class="form-control" type="date" name="date" id="date" placeholder="Jumlah Terjual"
                                value="{{ old('date') }}" autofocus required>
                        </div>

                        <div class="mb-3 col-md-7">
                            <label for="product_id" class="form-label">Barang</label>
                            <select class="form-select" name="product_id" id="product_id" required>
                                <option value="" disabled selected>Pilih</option>
                                @foreach ($products as $product)
                                <option value="{{ $product->id }}" @selected(old('product_id')==$product->id)>
                                    {{ $product->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3 col-md-12">
                            <label for="quantity" class="form-label">Jumlah</label>
                            <input class="form-control" type="number" min="1" name="quantity" id="quantity"
                                placeholder="Jumlah Terjual" value="{{ old('quantity') }}" required>
                        </div>


                    </div>

                    <div class="my-2">
                        <button type="submit" class="btn btn-primary px-5 me-2">Submit</button>
                        <a href="{{ route('transaction.index') }}" class="btn btn-label-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
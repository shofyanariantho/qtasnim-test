@extends('layouts.admin.app')
@section('title', 'Product')

@section('content')
<div class="row">
    <div class="col-md-9 mx-md-auto">
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between align-items-center">
                <div class="card-title mb-0">
                    <h5 class="mb-0">Create @yield('title')</h5>
                    <small class="text-muted">Buat @yield('title')</small>
                </div>
                <a href="{{ route('product.index') }}" class="btn p-0" title="Kembali">
                    <i class="ti ti-x ti-sm text-muted"></i>
                </a>
            </div>

            <div class="card-body">
                <form action="{{ route('product.store') }}" method="POST">
                    @csrf

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Terdapat beberapa masalah dengan inputan Anda.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="row">
                        <div class="mb-3 col-md-5">
                            <label for="category_id" class="form-label">Jenis Barang</label>
                            <select class="form-select" name="category_id" id="category_id" autofocus required>
                                <option value="" selected disabled>Pilih</option>
                                @foreach ($categories as $key => $category)
                                <option value="{{ $key }}" @selected(old('category_id')==$key)>{{ $category }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3 col-md-7">
                            <label for="name" class="form-label">Nama</label>
                            <input class="form-control" type="text" id="name" name="name" value="{{ old('name') }}"
                                placeholder="Nama Barang" required />
                        </div>

                        <div class="mb-3 col-md-12">
                            <label for="description" class="form-label">Deskripsi (Opsional)</label>
                            <textarea class="form-control" name="description" id="description" rows="3"
                                placeholder="Deskripsi Produk">{{ old('description') }}</textarea>
                        </div>

                        <div class="mb-3 col-md-12">
                            <label for="quantity" class="form-label">Stok</label>
                            <input class="form-control" type="number" min="0" id="quantity" name="quantity"
                                value="{{ old('quantity') }}" placeholder="Jumlah Stok" required />
                        </div>

                    </div>

                    <div class="my-2">
                        <button type="submit" class="btn btn-primary px-5 me-2">Submit</button>
                        <a href="{{ route('product.index') }}" class="btn btn-label-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.admin.app')
@section('title', 'Category')

@push('style')
<link rel="stylesheet" href="{{ asset('theme/assets/css/custom.css') }}" />
@endpush

@push('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<x-livewire-alert::scripts />
@endpush

@section('content')
@livewire('admin.show-category', ['title' => $__env->yieldContent('title')])
@endsection
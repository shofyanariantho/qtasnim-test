@extends('layouts.admin.app')
@section('title', 'Dashboard')

@push('style')
<link rel="stylesheet" href="{{ asset('theme/assets/vendor/libs/typeahead-js/typeahead.css') }}" />
<link rel="stylesheet" href="{{ asset('theme/assets/vendor/libs/apex-charts/apex-charts.css') }}" />
<link rel="stylesheet" href="{{ asset('theme/assets/vendor/libs/swiper/swiper.css') }}" />
<link rel="stylesheet" href="{{ asset('theme/assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css') }}" />
<link rel="stylesheet"
    href="{{ asset('theme/assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css') }}" />
<link rel="stylesheet"
    href="{{ asset('theme/assets/vendor/libs/datatables-checkboxes-jquery/datatables.checkboxes.css') }}" />
<link rel="stylesheet" href="{{ asset('theme/assets/vendor/css/pages/cards-advance.css') }}" />
<link rel="stylesheet" href="{{ asset('theme/assets/vendor/libs/apex-charts/apex-charts.css') }}" />
@endpush

@push('vendor-script')
<script src="{{ asset('theme/assets/vendor/libs/apex-charts/apexcharts.js') }}"></script>
@endpush

@section('content')
<div class="row">
    <div class="col-xl-4 mb-4 col-lg-5">
        <div class="card bg-primary text-white">
            <div class="d-flex align-items-end row">
                <div class="col-7">
                    <div class="card-body text-nowrap">
                        <h5 class="card-title text-white mb-0">{{ holderName() }}</h5>
                        <p class="mb-5">{{ appName() }}</p>
                        <a href="{{ route('transaction.index') }}" class="btn btn-light">Data Transaksi</a>
                    </div>
                </div>
                <div class="col-5 text-center text-sm-left">
                    <div class="card-body pb-0 px-0 px-md-4">
                        <img src="{{ asset('theme/assets/img/illustrations/card-advance-sale.png') }}" height="140"
                            alt="view sales">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-8 mb-4 col-lg-7 col-12">
        <div class="card h-100">
            <div class="card-header">
                <div class="d-flex justify-content-between mb-3">
                    <h5 class="card-title mb-0">Statistik Umum</h5>
                </div>
            </div>
            <div class="card-body">
                <div class="row gy-3">
                    <div class="col-md-4 col-6">
                        <div class="d-flex align-items-center">
                            <div class="badge rounded-pill bg-label-primary me-3 p-2">
                                <i class="ti ti-stack-2 ti-sm"></i>
                            </div>
                            <div class="card-info">
                                <h5 class="mb-0">{{ $category }}</h5>
                                <small>Jenis Barang</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="d-flex align-items-center">
                            <div class="badge rounded-pill bg-label-success me-3 p-2">
                                <i class="ti ti-milk ti-sm"></i>
                            </div>
                            <div class="card-info">
                                <h5 class="mb-0">{{ $product }}</h5>
                                <small>Barang</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="d-flex align-items-center">
                            <div class="badge rounded-pill bg-label-info me-3 p-2">
                                <i class="ti ti-shopping-cart ti-sm"></i>
                            </div>
                            <div class="card-info">
                                <h5 class="mb-0">{{ $transaction }}</h5>
                                <small>Transaksi</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-4 col-12">
        @livewire('admin.stat')
    </div>
</div>
@endsection
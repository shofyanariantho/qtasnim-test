<div>
    <div class="card">
        <div class="card-header border-bottom d-md-flex justify-content-md-between align-items-md-center">
            <div class="my-1 text-center text-md-start">
                <label>
                    <input wire:model.debounce.500ms="search" type="search" class="form-control" placeholder="Search..">
                </label>
            </div>
            <div class="text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column">
                <div class="my-1 me-md-2">
                    <label>
                        <select wire:model="paginate" class="form-select">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </label>
                </div>
                @can('create-transaction')
                <div class="flex-wrap my-1">
                    <a href="{{ route('transaction.create') }}"
                        class="btn btn-secondary text-white add-new btn-primary">
                        <span>
                            <i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
                            <span>
                                {{ $title }}
                            </span>
                        </span>
                    </a>
                </div>
                @endcan
            </div>
        </div>

        <div class="table-responsive">
            <table class="table border-top">
                <thead>
                    <tr>
                        <th class="text-nowrap">
                            <a wire:click="sort('date')" type="button"
                                class="d-flex justify-content-between align-items-center">
                                Tgl
                                @if($sortColumn['date'] === '')
                                <i class="ti ti-sort-ascending me-0 me-sm-1 ti-xs text-secondary"></i>
                                @elseif($sortColumn['date'] === 'asc')
                                <i class="ti ti-sort-descending me-0 me-sm-1 ti-xs text-secondary"></i>
                                @elseif($sortColumn['date'] === 'desc')
                                <i class="ti ti-arrows-sort me-0 me-sm-1 ti-xs text-muted" style='opacity: 0.5'></i>
                                @endif
                            </a>
                        </th>
                        <th>
                            <a wire:click="sort('product')" type="button"
                                class="d-flex justify-content-between align-items-center">
                                Barang
                                @if($sortColumn['product'] === '')
                                <i class="ti ti-sort-ascending me-0 me-sm-1 ti-xs text-secondary"></i>
                                @elseif($sortColumn['product'] === 'asc')
                                <i class="ti ti-sort-descending me-0 me-sm-1 ti-xs text-secondary"></i>
                                @elseif($sortColumn['product'] === 'desc')
                                <i class="ti ti-arrows-sort me-0 me-sm-1 ti-xs text-muted" style='opacity: 0.5'></i>
                                @endif
                            </a>
                        </th>
                        <th class="text-nowra">Jenis Barang</th>
                        <th class="text-center">Stok</th>
                        <th class="text-center text-nowrap">Jml Terjual</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($table as $key => $item)
                    @php
                    $updateRoute = route('transaction.update', $item->id);
                    @endphp

                    <tr wire:key="row{{ $item->id }}">
                        <td class="col-2">
                            {{ dateDMY($item->date) }}
                        </td>

                        <td class="col-3">
                            {{ $item->product->name }}
                        </td>

                        <td class="col-3">
                            {{ $item->product->category->name }}
                        </td>

                        <td class="col-2 text-center">
                            {{ $item->initial_stock }}
                        </td>

                        <td class="col-2 text-center">
                            {{ $item->quantity }}
                        </td>

                    </tr>
                    @empty
                    <div class="text-center col-md-7 mx-auto px-3 pt-3">
                        <div class="alert alert-secondary">
                            Data tidak ditemukan
                        </div>
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="card-body d-md-flex justify-content-md-between align-items-center pt-3 pb-2">
            <div class="align-self-start my-2 d-none d-md-block text-muted">
                <small>
                    Showing {{ $table->firstItem() }} to {{ $table->lastItem() }} of {{ $table->total() }} data
                </small>
            </div>
            {{ $table->links() }}
        </div>
    </div>

    <script>
        window.addEventListener('close-modal', event => {
            $('.dropdown-toggle').dropdown('hide');
            // $('#modalStatus').modal('hide');
        })
    </script>
</div>
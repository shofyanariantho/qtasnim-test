<div>
    <div class="card">
        <div class="card-header border-bottom d-md-flex justify-content-md-between align-items-md-center">
            <div class="my-1 text-center text-md-start">
                <label>
                    <input wire:model.debounce.500ms="search" type="search" class="form-control" placeholder="Search..">
                </label>
            </div>
            <div class="text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column">
                <div class="my-1 me-md-2">
                    <label>
                        <select wire:model="paginate" class="form-select">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </label>
                </div>
                @can('create-category')
                <div class="flex-wrap my-1">
                    <a href="{{ route('category.create') }}" class="btn btn-secondary text-white add-new btn-primary">
                        <span>
                            <i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
                            <span>
                                {{ $title }}
                            </span>
                        </span>
                    </a>
                </div>
                @endcan
            </div>
        </div>

        <div class="table-responsive">
            <table class="table border-top">
                <thead>
                    <tr>
                        <th>Jenis Barang</th>
                        <th class="text-center">Jumlah Barang</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($table as $key => $item)
                    @php
                    $updateRoute = route('category.update', $item->id);
                    @endphp

                    <tr wire:key="row{{ $item->id }}">
                        <td class="col-6">
                            {{ $item->name }}
                        </td>

                        <td class="col-6 text-center">
                            {{ $item->products()->count() }}
                        </td>

                        <td>
                            <div class="d-flex align-items-center">
                                @can('update-category')
                                <a href="{{ route('category.edit', $item->id) }}" class="action-btn" title="edit">
                                    <i class="ti ti-edit ti-sm me-2 fs-5"></i>
                                </a>
                                @endcan

                                @can('delete-category')
                                <a href="javascript:;" class="action-btn" title="delete" data-bs-toggle="modal"
                                    data-bs-target="#modalDelete{{ $item->id }}">
                                    <i class="ti ti-trash ti-sm mx-2 fs-5"></i>
                                </a>
                                @include('admin.modal.delete')
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @empty
                    <div class="text-center col-md-7 mx-auto px-3 pt-3">
                        <div class="alert alert-secondary">
                            Data tidak ditemukan
                        </div>
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="card-body d-md-flex justify-content-md-between align-items-center pt-3 pb-2">
            <div class="align-self-start my-2 d-none d-md-block text-muted">
                <small>
                    Showing {{ $table->firstItem() }} to {{ $table->lastItem() }} of {{ $table->total() }} data
                </small>
            </div>
            {{ $table->links() }}
        </div>
    </div>

    <script>
        window.addEventListener('close-modal', event => {
            $('.dropdown-toggle').dropdown('hide');
            // $('#modalStatus').modal('hide');
        })
    </script>
</div>
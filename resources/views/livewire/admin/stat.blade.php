<div class="card">
    <div class="card-header">
        <h5 class="card-title mb-3">Rekap Transaksi</h5>
        <div class="row ">
            <div class="col-md-3">
                <select wire:model="category_id" id="UserPlan" class="form-select text-capitalize">
                    <option value="">Semua Jenis Barang</option>
                    @foreach ($categories as $key => $category)
                    <option value="{{ $key }}">{{ $category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <input wire:model="start_date" class="form-control" type="date">
            </div>
            <div class="col-md-3">
                <input wire:model="end_date" class="form-control" type="date">
            </div>

        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table border-top">
                <thead>
                    <tr>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th class="text-center">Terjual</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $key => $item)
                    <tr wire:key="row{{ $key }}">
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->product->category->name }}</td>
                        <td class="text-center">{{ $item->total_qty }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
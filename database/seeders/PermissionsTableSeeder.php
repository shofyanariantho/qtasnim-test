<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'create-user-admin',
                'group' => 'User',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:02:18',
                'updated_at' => '2023-04-13 07:02:18',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'read-user-admin',
                'group' => 'User',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:02:27',
                'updated_at' => '2023-04-13 07:02:27',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'update-user-admin',
                'group' => 'User',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:02:34',
                'updated_at' => '2023-04-13 07:02:34',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'delete-user-admin',
                'group' => 'User',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:02:41',
                'updated_at' => '2023-04-13 07:02:41',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'create-role',
                'group' => 'Role',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:02:50',
                'updated_at' => '2023-04-13 07:02:50',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'read-role',
                'group' => 'Role',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:02:55',
                'updated_at' => '2023-04-13 07:02:55',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'update-role',
                'group' => 'Role',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:03:28',
                'updated_at' => '2023-04-13 07:03:28',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'delete-role',
                'group' => 'Role',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:03:34',
                'updated_at' => '2023-04-13 07:03:34',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'create-permission',
                'group' => 'Permission',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:04:19',
                'updated_at' => '2023-04-13 07:04:19',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'read-permission',
                'group' => 'Permission',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:04:27',
                'updated_at' => '2023-04-13 07:04:27',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'update-permission',
                'group' => 'Permission',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:04:36',
                'updated_at' => '2023-04-13 07:04:36',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'delete-permission',
                'group' => 'Permission',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 07:04:42',
                'updated_at' => '2023-04-13 07:04:42',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'create-menu',
                'group' => 'Menu',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 08:15:00',
                'updated_at' => '2023-04-13 08:15:00',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'read-menu',
                'group' => 'Menu',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 08:15:00',
                'updated_at' => '2023-04-13 08:15:00',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'update-menu',
                'group' => 'Menu',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 08:15:00',
                'updated_at' => '2023-04-13 08:15:00',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'delete-menu',
                'group' => 'Menu',
                'guard_name' => 'web',
                'created_at' => '2023-04-13 08:15:00',
                'updated_at' => '2023-04-13 08:15:00',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'create-setting',
                'group' => 'Setting',
                'guard_name' => 'web',
                'created_at' => '2023-04-24 07:47:09',
                'updated_at' => '2023-04-24 07:47:09',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'read-setting',
                'group' => 'Setting',
                'guard_name' => 'web',
                'created_at' => '2023-04-24 07:47:20',
                'updated_at' => '2023-04-24 07:47:20',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'update-setting',
                'group' => 'Setting',
                'guard_name' => 'web',
                'created_at' => '2023-04-24 07:47:31',
                'updated_at' => '2023-04-24 07:47:31',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'delete-setting',
                'group' => 'Setting',
                'guard_name' => 'web',
                'created_at' => '2023-04-24 07:47:40',
                'updated_at' => '2023-04-24 07:47:40',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'create-category',
                'group' => 'Category',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:22:17',
                'updated_at' => '2023-06-09 14:22:17',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'read-category',
                'group' => 'Category',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:22:17',
                'updated_at' => '2023-06-09 14:22:17',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'update-category',
                'group' => 'Category',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:22:17',
                'updated_at' => '2023-06-09 14:22:17',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'delete-category',
                'group' => 'Category',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:22:17',
                'updated_at' => '2023-06-09 14:22:17',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'create-product',
                'group' => 'Product',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:38:52',
                'updated_at' => '2023-06-09 14:38:52',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'read-product',
                'group' => 'Product',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:38:52',
                'updated_at' => '2023-06-09 14:38:52',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'update-product',
                'group' => 'Product',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:38:52',
                'updated_at' => '2023-06-09 14:38:52',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'delete-product',
                'group' => 'Product',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 14:38:52',
                'updated_at' => '2023-06-09 14:38:52',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'create-transaction',
                'group' => 'Transaction',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 16:13:46',
                'updated_at' => '2023-06-09 16:13:46',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'read-transaction',
                'group' => 'Transaction',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 16:13:46',
                'updated_at' => '2023-06-09 16:13:46',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'update-transaction',
                'group' => 'Transaction',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 16:13:46',
                'updated_at' => '2023-06-09 16:13:46',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'delete-transaction',
                'group' => 'Transaction',
                'guard_name' => 'web',
                'created_at' => '2023-06-09 16:13:46',
                'updated_at' => '2023-06-09 16:13:46',
            ),
        ));
        
        
    }
}
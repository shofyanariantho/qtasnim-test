<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Konsumsi',
                'created_at' => '2023-06-09 14:29:57',
                'updated_at' => '2023-06-09 14:29:57',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Pembersih',
                'created_at' => '2023-06-09 14:34:01',
                'updated_at' => '2023-06-09 14:34:01',
            ),
        ));
        
        
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 3,
                'date' => '2021-05-01',
                'product_id' => 1,
                'initial_stock' => 100,
                'quantity' => 10,
                'final_stock' => 90,
                'created_at' => '2023-06-09 17:26:16',
                'updated_at' => '2023-06-09 17:26:16',
            ),
            1 => 
            array (
                'id' => 4,
                'date' => '2021-05-05',
                'product_id' => 2,
                'initial_stock' => 100,
                'quantity' => 19,
                'final_stock' => 81,
                'created_at' => '2023-06-09 17:28:01',
                'updated_at' => '2023-06-09 17:28:01',
            ),
            2 => 
            array (
                'id' => 5,
                'date' => '2021-05-10',
                'product_id' => 1,
                'initial_stock' => 90,
                'quantity' => 15,
                'final_stock' => 75,
                'created_at' => '2023-06-09 17:28:34',
                'updated_at' => '2023-06-09 17:28:34',
            ),
            3 => 
            array (
                'id' => 6,
                'date' => '2021-05-11',
                'product_id' => 3,
                'initial_stock' => 100,
                'quantity' => 20,
                'final_stock' => 80,
                'created_at' => '2023-06-09 17:28:59',
                'updated_at' => '2023-06-09 17:28:59',
            ),
            4 => 
            array (
                'id' => 7,
                'date' => '2021-05-11',
                'product_id' => 4,
                'initial_stock' => 100,
                'quantity' => 30,
                'final_stock' => 70,
                'created_at' => '2023-06-09 17:29:59',
                'updated_at' => '2023-06-09 17:29:59',
            ),
            5 => 
            array (
                'id' => 8,
                'date' => '2021-05-12',
                'product_id' => 5,
                'initial_stock' => 100,
                'quantity' => 25,
                'final_stock' => 75,
                'created_at' => '2023-06-09 17:30:43',
                'updated_at' => '2023-06-09 17:30:43',
            ),
            6 => 
            array (
                'id' => 9,
                'date' => '2021-05-12',
                'product_id' => 2,
                'initial_stock' => 81,
                'quantity' => 5,
                'final_stock' => 76,
                'created_at' => '2023-06-09 17:31:08',
                'updated_at' => '2023-06-09 17:31:08',
            ),
        ));
        
        
    }
}
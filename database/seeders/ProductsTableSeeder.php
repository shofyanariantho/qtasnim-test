<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Kopi',
                'description' => 'Kopi Hitam',
                'category_id' => 1,
                'created_at' => '2023-06-09 14:55:47',
                'updated_at' => '2023-06-09 15:03:30',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Teh',
                'description' => NULL,
                'category_id' => 1,
                'created_at' => '2023-06-09 14:59:38',
                'updated_at' => '2023-06-09 14:59:38',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Pasta Gigi',
                'description' => NULL,
                'category_id' => 2,
                'created_at' => '2023-06-09 14:59:50',
                'updated_at' => '2023-06-09 14:59:50',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Sabun Mandi',
                'description' => NULL,
                'category_id' => 2,
                'created_at' => '2023-06-09 15:03:46',
                'updated_at' => '2023-06-09 15:03:46',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Sampo',
                'description' => NULL,
                'category_id' => 2,
                'created_at' => '2023-06-09 15:03:59',
                'updated_at' => '2023-06-09 15:03:59',
            ),
        ));
        
        
    }
}
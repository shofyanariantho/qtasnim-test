<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StocksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('stocks')->delete();
        
        \DB::table('stocks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'product_id' => 1,
                'quantity' => 75,
                'created_at' => '2023-06-09 15:50:24',
                'updated_at' => '2023-06-09 17:28:34',
            ),
            1 => 
            array (
                'id' => 2,
                'product_id' => 2,
                'quantity' => 76,
                'created_at' => '2023-06-09 15:51:50',
                'updated_at' => '2023-06-09 17:31:08',
            ),
            2 => 
            array (
                'id' => 3,
                'product_id' => 3,
                'quantity' => 80,
                'created_at' => '2023-06-09 15:52:12',
                'updated_at' => '2023-06-09 17:28:59',
            ),
            3 => 
            array (
                'id' => 4,
                'product_id' => 4,
                'quantity' => 70,
                'created_at' => '2023-06-09 15:52:50',
                'updated_at' => '2023-06-09 17:29:59',
            ),
            4 => 
            array (
                'id' => 5,
                'product_id' => 5,
                'quantity' => 75,
                'created_at' => '2023-06-09 15:53:01',
                'updated_at' => '2023-06-09 17:30:43',
            ),
        ));
        
        
    }
}
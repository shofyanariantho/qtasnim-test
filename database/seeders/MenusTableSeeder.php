<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Administrator',
                'url' => 'admin/user',
                'permission_id' => 2,
                'icon' => NULL,
                'main_menu' => 6,
                'sort' => 1,
                'created_at' => '2023-04-13 10:45:28',
                'updated_at' => '2023-06-08 06:17:37',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Role',
                'url' => 'admin/role',
                'permission_id' => 6,
                'icon' => NULL,
                'main_menu' => 5,
                'sort' => 2,
                'created_at' => '2023-04-13 11:12:44',
                'updated_at' => '2023-06-08 06:17:42',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Permission',
                'url' => 'admin/permission',
                'permission_id' => 10,
                'icon' => NULL,
                'main_menu' => 5,
                'sort' => 3,
                'created_at' => '2023-04-13 11:45:17',
                'updated_at' => '2023-06-08 06:17:06',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Menu',
                'url' => 'admin/menu',
                'permission_id' => 14,
                'icon' => NULL,
                'main_menu' => 5,
                'sort' => 4,
                'created_at' => '2023-04-13 11:43:45',
                'updated_at' => '2023-06-08 06:17:13',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Setting',
                'url' => NULL,
                'permission_id' => NULL,
                'icon' => 'adjustments-horizontal',
                'main_menu' => NULL,
                'sort' => 5,
                'created_at' => '2023-04-24 07:49:08',
                'updated_at' => '2023-06-09 16:17:34',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Users',
                'url' => NULL,
                'permission_id' => NULL,
                'icon' => 'users',
                'main_menu' => NULL,
                'sort' => 4,
                'created_at' => '2023-04-25 09:45:47',
                'updated_at' => '2023-06-09 16:17:34',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'App',
                'url' => 'admin/setting',
                'permission_id' => 18,
                'icon' => NULL,
                'main_menu' => 5,
                'sort' => 1,
                'created_at' => '2023-05-23 10:08:35',
                'updated_at' => '2023-06-08 06:17:42',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Jenis Barang',
                'url' => 'admin/category',
                'permission_id' => 22,
                'icon' => 'stack-2',
                'main_menu' => NULL,
                'sort' => 3,
                'created_at' => '2023-06-09 14:24:30',
                'updated_at' => '2023-06-09 16:17:34',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Barang',
                'url' => 'admin/product',
                'permission_id' => 26,
                'icon' => 'milk',
                'main_menu' => NULL,
                'sort' => 2,
                'created_at' => '2023-06-09 14:41:53',
                'updated_at' => '2023-06-09 16:17:34',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Transaksi',
                'url' => 'admin/transaction',
                'permission_id' => 30,
                'icon' => 'shopping-cart',
                'main_menu' => NULL,
                'sort' => 1,
                'created_at' => '2023-06-09 16:17:30',
                'updated_at' => '2023-06-09 16:17:30',
            ),
        ));
        
        
    }
}
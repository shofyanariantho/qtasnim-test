<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'app_name' => 'Qtasnim',
                'app_version' => NULL,
                'name' => 'Shofyan Ariantho',
                'address' => 'Sukabumi, Jawa Barat',
                'logo' => '',
                'created_at' => '2023-05-11 14:46:48',
                'updated_at' => '2023-06-09 14:17:25',
            ),
        ));
        
        
    }
}
<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'name' => 'Webdev',
                'username' => 'webdev',
                'email' => 'webdev@shofyan.my.id',
                'password' => '$2y$10$kpfYig8ZOtKlXnc6dmCR9O4HTifCsBT4ETHtgPWTcQKQWCn2I8p1C',
            ],
            [
                'name' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@test.my.id',
                'password' => '$2y$10$uaQL4oUbG8K522NDB/CwauyjyVisL6IGXLapG1b0zt9bAYU5IErSi',
            ],
        ]);
        
        $superAdmin = User::find(1);
        $admin = User::find(2);

        $superAdmin->assignRole('Super Admin');
        $admin->assignRole('Admin');       
    }
}

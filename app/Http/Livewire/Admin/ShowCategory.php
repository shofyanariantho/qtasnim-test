<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Category as Model;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class ShowCategory extends Component
{
    use LivewireAlert;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $paginationClasses = 'd-flex align-items-center';

    public $paginate = 10;
    public $search = "";

    public $title, $model, $modelId;

    public function mount(Model $model)
    {
        $this->model = $model;
    }

    public function render()
    {
        $table = $this->model
            ->filter($this->search)
            ->paginate($this->paginate);

        return view('livewire.admin.show-category', [
            'table' => $table,
        ]);
    }

}
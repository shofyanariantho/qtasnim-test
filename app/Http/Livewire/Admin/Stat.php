<?php

namespace App\Http\Livewire\Admin;

use App\Models\Product;
use Livewire\Component;
use App\Models\Category;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class Stat extends Component
{
    public $category_id, $start_date, $end_date;

    public function render()
    {   
        $categories = Category::pluck('name', 'id');
        
        $transactions = Transaction::select('product_id', DB::raw('SUM(quantity) as total_qty'))
            ->groupBy('product_id')
            ->when($this->category_id, function ($query) {
                $query->whereHas('product', function($query) {
                    $query->where('category_id', $this->category_id);
                });
            })
            ->when($this->end_date > $this->start_date, function($query) {
                $query->whereBetween('date', [$this->start_date, $this->end_date]);
            })
            ->orderBy('total_qty', 'desc')
            ->get();
        
        return view('livewire.admin.stat', compact('categories', 'transactions'));
    }
}

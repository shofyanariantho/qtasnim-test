<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Transaction as Model;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class ShowTransaction extends Component
{
    use LivewireAlert;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $paginationClasses = 'd-flex align-items-center';

    public $paginate = 10;
    public $search = "";
    public $sortColumn = [
        'product' => '',
        'date' => '',
    ];

    public $title, $model, $modelId;

    public function mount(Model $model)
    {
        $this->model = $model;
    }

    public function sort($col)
    {
        if ($col) {
            if ($this->sortColumn[$col] === '') {
                $this->sortColumn[$col] = 'asc';
            } elseif ($this->sortColumn[$col] === 'asc') {
                $this->sortColumn[$col] = 'desc';
            } elseif ($this->sortColumn[$col] === 'desc') {
                $this->sortColumn[$col] = '';
            }
        }
    }

    public function render()
    {
        $table = $this->model
            ->join('products', 'transactions.product_id', '=', 'products.id')
            ->select('transactions.*')
            ->filter($this->search)
            ->when($this->sortColumn['product'], function ($query) {
                $query->orderBy('products.name', $this->sortColumn['product']);
            })
            ->when($this->sortColumn['date'], function ($query) {
                $query->orderBy('date', $this->sortColumn['date']);
            })
            ->when(empty($this->sortColumn['date']), function ($query) {
                $query->orderBy('date', 'asc');
            })
            ->paginate($this->paginate);

        return view('livewire.admin.show-transaction', [
            'table' => $table,
        ]);
    }

}
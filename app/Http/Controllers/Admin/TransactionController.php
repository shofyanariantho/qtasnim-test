<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Transaction as Model;

class TransactionController extends Controller
{
    public $model, $view, $route;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->view = 'admin.transaction';
        $this->route = 'transaction';

        $this->middleware('can:create-' . $this->route)->only('create','store');
        $this->middleware('can:read-' . $this->route)->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::whereHas('stock', function($query) {
                $query->where('quantity', '>', 0);
            })->select('id', 'name')->get();
        return view($this->view . '.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'date' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
        ]);

        $product = Product::find($data['product_id']);
        $data['initial_stock'] = $product->stock->quantity;
        $data['final_stock'] = $data['initial_stock'] - $data['quantity'];
        
        DB::transaction(function () use($data, $product) {
            $this->model->create($data);
            $product->stock()->update([
                'quantity' => $data['final_stock']
            ]);
        });
        
        alertNotif('save');
        return redirect()->route($this->route . '.index');
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\School;
use App\Models\Product;
use App\Models\Category;
use App\Models\Classroom;
use App\Models\Transaction;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $category = Category::count();
        $product = Product::count();
        $transaction = Transaction::count();
        return view('admin.dashboard.dashboard', compact('category', 'product', 'transaction'));
    }
}

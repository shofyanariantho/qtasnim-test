<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Product as Model;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $model, $view, $route;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->view = 'admin.product';
        $this->route = 'product';

        $this->middleware('can:create-' . $this->route)->only('create','store');
        $this->middleware('can:read-' . $this->route)->only('index');
        $this->middleware('can:update-' . $this->route)->only('edit','update');
        $this->middleware('can:delete-' . $this->route)->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Category::pluck('name', 'id');
        return view($this->view . '.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'category_id' => 'required',
            'name' => 'required|unique:products,name',
            'description' => 'nullable',
            'quantity' => 'nullable',
        ]);

        DB::transaction(function () use($data) {
            $model = $this->model->create($data);
            $model->stock()->create([
                'quantity' => $data['quantity']
            ]);
        });

        alertNotif('save');
        return redirect()->route($this->route . '.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->model->findOrFail($id);
        $categories = Category::pluck('name', 'id');
        return view($this->view . '.edit', compact('data', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = $this->model->find($id);
        $data = $request->validate([
            'category_id' => 'required',
            'name' => 'required|unique:products,name,'.$id,
            'description' => 'nullable',
            'quantity' => 'nullable',
        ]);

        DB::transaction(function () use($model, $data) {
            $model->update($data);
            $model->stock()->update([
                'quantity' => $data['quantity']
            ]);
        });

        alertNotif('update');
        return redirect()->route($this->route . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->model->find($id);

        DB::transaction(function () use($model) {
            $model->stock()->delete();
            $model->delete();
        });

        alertNotif('delete');
        return redirect()->route($this->route . '.index');
    }
}

<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    /**
     * Get the product that owns the Transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function scopeFilter($query, $search) 
    {
        $query->when($search ?? false, function($query, $search){
            $query->whereHas('product', function($query) use($search) {
                return $query->where('name', 'like', "%$search%");
            });
        });
    }
}

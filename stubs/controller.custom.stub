<?php

namespace {{ namespace }};

use {{ namespacedModel }} as Model;
use {{ rootNamespace }}Http\Controllers\Controller;
use {{ namespacedRequests }}

class {{ class }} extends Controller
{
    public $model, $view, $route;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->view = '';
        $this->route = '';

        // $this->middleware('can:create-' . $this->route)->only('create','store');
        // $this->middleware('can:read-' . $this->route)->only('index');
        // $this->middleware('can:update-' . $this->route)->only('edit','update');
        // $this->middleware('can:delete-' . $this->route)->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view . '.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \{{ namespacedStoreRequest }}  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            //
        ]);

        $this->model->create($data);
        alertNotif('save');

        return redirect()->route($this->route . '.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \{{ namespacedModel }}  ${{ modelVariable }}
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->model->findOrFail($id);
        return view($this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \{{ namespacedUpdateRequest }}  $request
     * @param  \{{ namespacedModel }}  ${{ modelVariable }}
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = $this->model->find($id);
        $data = $request->validate([
            //
        ]);

        $data['updated_by'] = auth()->user()->id;
        $model->update($data);
        alertNotif('update');

        return redirect()->route($this->route . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->model->find($id);
        $model->delete();
        alertNotif('delete');

        return redirect()->route($this->route . '.index');
    }
}
